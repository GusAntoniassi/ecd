# Estrutura e Classificação de Dados
Repositório com resoluções das listas de Estrutura e Classificação de Dados do curso de Sistemas De Informação da UNIPAR - Umuarama, ano de 2016.

## 1º Bimestre
### 1º Ano:
* [Lista 01 / Permuta 1](https://bitbucket.org/GusAntoniassi/ecd/src/master/1o%20Ano/Lista_01/?at=master)
* [Lista 02 / Permuta 3](https://bitbucket.org/GusAntoniassi/ecd/src/master/1o%20Ano/Lista_02/?at=master)
* [Lista 03a / Permuta 4](https://bitbucket.org/GusAntoniassi/ecd/src/master/1o%20Ano/Lista_03a/?at=master)

### 2º Ano:
* [Lista 03](https://bitbucket.org/GusAntoniassi/ecd/src/master/2o%20Ano/Lista_03/?at=master)
* [Lista 03a / Permuta 1](https://bitbucket.org/GusAntoniassi/ecd/src/master/2o%20Ano/Lista_03a/?at=master)

##2º Bimestre:
###1º Ano:
* [Permuta 1](https://bitbucket.org/GusAntoniassi/ecd/src/master/1%C2%BA%20Ano/2%C2%BA%20Bimestre/Permuta_1/?at=master)
* [Lista 07a](https://bitbucket.org/GusAntoniassi/ecd/src/master/1%C2%BA%20Ano/2%C2%BA%20Bimestre/Lista_07a/?at=master)
* [Lista 08a](https://bitbucket.org/GusAntoniassi/ecd/src/master/1%C2%BA%20Ano/2%C2%BA%20Bimestre/Lista_08a/?at=master)
* [Lista 08b](https://bitbucket.org/GusAntoniassi/ecd/src/master/1%C2%BA%20Ano/2%C2%BA%20Bimestre/Lista_08b/?at=master)
* [Lista 10a](https://bitbucket.org/GusAntoniassi/ecd/src/master/1%C2%BA%20Ano/2%C2%BA%20Bimestre/Lista_10a/?at=master)
* [Lista 10b](https://bitbucket.org/GusAntoniassi/ecd/src/master/1%C2%BA%20Ano/2%C2%BA%20Bimestre/Lista_10b/?at=master)
* [Lista 16a](https://bitbucket.org/GusAntoniassi/ecd/src/master/1%C2%BA%20Ano/2%C2%BA%20Bimestre/Lista_16a/?at=master)
* [Lista 16b](https://bitbucket.org/GusAntoniassi/ecd/src/master/1%C2%BA%20Ano/2%C2%BA%20Bimestre/Lista_16b/?at=master)
* [Lista 18a](https://bitbucket.org/GusAntoniassi/ecd/src/master/1%C2%BA%20Ano/2%C2%BA%20Bimestre/Lista_18a/?at=master)
* [Lista 18b](https://bitbucket.org/GusAntoniassi/ecd/src/master/1%C2%BA%20Ano/2%C2%BA%20Bimestre/Lista_18b/?at=master)
* [Permuta 2](https://bitbucket.org/GusAntoniassi/ecd/src/master/1%C2%BA%20Ano/2%C2%BA%20Bimestre/Permuta_2/?at=master)