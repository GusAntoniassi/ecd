/*3 – Ler uma variável, somar 5 e multiplicar por 2.5 caso seja par ou somar 8 e dividir por 3 caso seja
ímpar, imprimir o resultado desta operação. Pesquise na internet, livros, apostilas, como se faz para
verificar se o número é par ou ímpar na linguagem C.*/

int main() {
    // Declarar as variáveis
    int variavel, resultado;

    // Ler uma variável
    printf("Insira um número: ");
    scanf("%i", &variavel);

    /* Para saber se um número é par ou não em linguagem C, utilizamos o operador Módulo (ou Resto da Divisão).
    O operador pega o número da esquerda, divide pelo número da direita e retorna o resto da divisão.

    Ex: 3/2 é 1, e o resto da divisão é 1.

    Portanto, para saber se um número é par ou ímpar, podemos dividi-lo por 2 e analisar o resto da divisão.
    Todo número dividido por 2 terá resto 0 ou 1, com 0 se for par e 1 se for ímpar. */

    // Se o resto da divisão da variável por 2 for zero, o número é par
    if (variavel % 2 == 0) {
        // Somar 5 e multiplicar por 2.5
        printf("Número par -- Somando por 5 e multiplicando por 2.5...\n\n");
        resultado = (variavel + 5) * 2.5; // Utilizamos parênteses para indicar que queremos somar primeiro e multiplicar depois
    } else {
        // Somar 8 e dividir por 3
        printf("Número ímpar -- Somando por 8 e dividindo por 3...\n\n");
        resultado = (variavel + 8) / 3;
    }

    // Imprimir o resultado da operação
    printf("O resultado da operação é: %i", resultado);

    return 0;
}
