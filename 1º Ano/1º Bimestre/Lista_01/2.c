/*2 – Ler três variáveis (A, B e C), se A for negativo faça X=(A+B+C)/2, caso contrário X=(A+B+C)/3.
No final imprima o valor de X*/
#include <stdio.h>

int main() {
    // Declaramos as variáveis
    int A, B, C, X;

    // Lemos as três variáveis
    printf("Insira um valor para A: ");
    scanf("%i", &A);

    printf("Insira um valor para B: ");
    scanf("%i", &B);

    printf("Insira um valor para C: ");
    scanf("%i", &C);

    // Se A for menor que zero, ou seja, negativo
    if (A < 0) {
        // Fazer X=(A+B+C)/2
        X = (A+B+C)/2;

    // Caso contrário
    } else {
        // Fazer X=(A+B+C)/3
        X = (A+B+C)/3;
    }

    // Imprimir o valor de X
    printf("O valor de X é: %i", X);

    return 0;
}
