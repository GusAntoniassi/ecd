/*1 – Encontrar, o dobro de um número caso ele seja positivo o seu triplo caso seja negativo e se for igual
a zero informar ao usuário o valor informado. Antes de mostrar o resultado escrever uma mensagem
indicando o resultado.*/

#include <stdio.h>

int main() {
    // Declaramos a variável que vai armazenar o número digitado
    int numero;

    // Mostramos uma mensagem para orientar o usuário
    printf("Digite um número: ");
    // Lemos o número
    scanf("%i", &numero);

    // Se o número digitado pelo usuário for zero
    if (numero == 0) {
        // Informamos ao usuário o valor informado
        printf("Você digitou 0");

    // Se o número for maior que zero, ou seja, positivo
    } else if (numero > 0) {
        // Retornamos o dobro do número
        printf("Número positivo - o dobro é: %i", numero * 2);

    // Se o número não for zero e nem positivo, então ele é negativo, portanto não precisamos usar "else if"
    } else {
        printf("Número negativo - o triplo é: %i", numero * 3);
    }

    return 0;
}
