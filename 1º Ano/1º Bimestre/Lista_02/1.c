/*1. Leia um valor inteiro entre 1 e 12 e assuma que esse valor representa um mês. Após a entrada do
valor escreva o nome deste mês baseando-se no numero digitado, ou seja se o usuário entrar com o
numero 6 o algoritmo deve escrever JUNHO. Caso seja digitado um valor inválido para mês, avisar o
usuário que foi digitado um mês inválido. Utilize a estrutura de decisão de múltipla escolha.*/

#include <locale.h> // Biblioteca necess�ria para usar acentua��o

int main() {
	setlocale(LC_ALL, ""); // Instru��o necess�ria para usar acentua��o
	
    // Decalaração das variáveis
    int valor;

    // Ler o valor
    printf("Insira um valor entre 1 e 12: ");
    scanf("%i", &valor);

    // Utilizando a estrutura de múltipla escolha
    switch (valor) {
        case 1: // Se foi digitado o número 1, imprimir "Janeiro"
            printf("Janeiro");
            break;
        case 2: // Se foi digitado o número 2, imprimir "Fevereiro
            printf("Fevereiro");
            break;
        case 3: // Assim por diante...
            printf("Mar�o");
            break;
        case 4:
            printf("Abril");
            break;
        case 5:
            printf("Maio");
            break;
        case 6:
            printf("Junho");
            break;
        case 7:
            printf("Julho");
            break;
        case 8:
            printf("Agosto");
            break;
        case 9:
            printf("Setembro");
            break;
        case 10:
            printf("Outubro");
            break;
        case 11:
            printf("Novembro");
            break;
        case 12:
            printf("Dezembro");
            break;
        default: // Se for digitado qualquer outra tecla
            printf("Foi digitado um mês inválido!");
    }

    return 0;
}
