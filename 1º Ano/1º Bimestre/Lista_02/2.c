/*2. Elabore um algoritmo que monte um menu com as seguintes op��es:
A - Exerc�cio 1
B - Exerc�cio 2
F - Fim
Caso o usu�rio escolha a op��o A, imprima a seguinte mensagem: "Bom dia, voc� entrou no exerc�cio 1"�.
Ap�s isso, encerre o algoritmo.

Caso o usu�rio escolha a op��o B, imprima a seguinte mensagem: "Boa noite voc� entrou no exerc�cio 2"�.
Ap�s isso, encerre o algoritmo.

Caso o usu�rio escolha a op��o F, imprima a seguinte mensagem: "At� mais, aqui � o final da execu��o
do algoritmo"�. Ap�s isso, encerre o algoritmo.

Caso o usu�rio digite algo diferente de A, B ou F, imprima a seguinte mensagem: "Valor inv�lido"� . Ap�s
isso, encerre o algoritmo.*/

#include <stdio.h>
#include <locale.h> // Biblioteca necess�ria para usar acentua��o

int main() {
	setlocale(LC_ALL, ""); // Instru��o necess�ria para usar acentua��o
    char tecla;

    // Imprimimos o menu
    printf("A - Exerc�cio 1\n");
    printf("B - Exerc�cio 2\n");
    printf("F - Fim\n");
    printf("Escolha uma op��o...\n");

    // Pegamos a tecla digitada com a fun��o getch()
    tecla = getch();

    // Estrutura de m�ltipla escolha 
    switch (tecla) {
        /*
            Quando pedimos ao usu�rio para escolher uma op��o por meio de letras, ele pode entrar com
            uma tecla min�scula ('a') ou uma tecla mai�scula ('A'). Para o compilador, s�o dois caracteres
            diferentes.
            Por essa raz�o, nas estruturas de decis�o/m�ltipla escolha � sempre uma boa pr�tica verificar
            pelo caractere digitado tanto na forma mai�scula quanto na forma min�scula. Para fazer isso podemos:
                1) Usar duas instru��es 'case' seguidas (m�todo adotado aqui)
                2) Usar a fun��o tolower(tecla). Note que isso requer a biblioteca "ctype", que deve ser inclu�da
                atrav�s de um #include <ctype.h> no come�o do arquivo.
        */

        // Exerc�cio 1
        case 'a': // Note o uso de aspas simples ('a') e n�o aspas duplas ("a"), pois estamos usando chars, e n�o strings.
        case 'A':
            printf("Bom dia, voc� entrou no exerc�cio 1");
            break;

        // Exerc�cio 2
        case 'b':
        case 'B':
            printf("Boa noite, voc� entrou no exerc�cio 2");
            break;

        // Fim
        case 'f':
        case 'F':
            printf("At� mais, aqui � o final da execu��o do algoritmo");
            break;

        // Qualquer outra tecla
        default:
            printf("Valor inv�lido");
    }


    return 0;
}
