/*3) Uma empresa ir� dar um aumento de sal�rio aos seus funcion�rios de acordo com a categoria de
cada empregado. O aumento seguir� a seguinte regra:
    Funcion�rios das categorias A, B, C, e D ganhar�o 10% de aumento sobre o sal�rio
    Funcion�rios das categorias E ganhar�o 15% de aumento sobre o sal�rio
    Funcion�rios das categorias F e G ganhar�o 25% de aumento sobre o sal�rio
    Funcion�rios das categorias H, I e J ganhar�o 35% de aumento sobre o sal�rio
    Funcion�rios das categorias K, L, M e N ganhar�o 50% de aumento sobre o sal�rio
    Funcion�rios de outras categorias que n�o se enquadram nas anteriores n�o ter�o
aumento, favor informar o funcion�rio com uma mensagem ao executar o algoritmo.*/

#include <stdio.h>
#include <locale.h> // Biblioteca necess�ria para usar acentua��o

int main() {
	setlocale(LC_ALL, ""); // Instru��o necess�ria para usar acentua��o
	
	// Declaramos as vari�veis
	char categoria; // Char representando a categoria do funcion�rio
	float salario, novoSalario; // Sal�rio e Novo Sal�rio devem ser do tipo float por causa dos centavos
	int aumento; // Porcentagem de aumento salarial
	
	// Ler a categoria do funcion�rio
	printf("Insira a categoria do funcion�rio \n");
	categoria = getche();
	
	// Ler o sal�rio do funcion�rio
	printf("\nInsira o sal�rio atual do funcion�rio (sem v�rgulas ou pontos)\n");
	scanf("%f", &salario); // Aqui deve-se usar %f em vez do %i, pois estamos lendo um valor float.
	
	// Estrutura de m�ltipla escolha
	switch(categoria) {
		/* 	
			Aqui utilizamos o conceito dos m�ltiplos casos (visto no exerc�cio anterior, para tratar
			tanto letras min�sculas quanto mai�sculas), e tamb�m o conceito de intervalos.
			
			Em vez de escrever:
			case 'a':
			case 'b':
			case 'c':
			etc...
			
			Podemos utilizar retic�ncias para indicar um intervalo. Portanto 'a'...'c' engloba 'a', 'b' e 'c'.
			
			Tamb�m podemos usar intervalos com n�meros, com 1..5 englobando 1, 2, 3, 4 e 5.
		*/
		
		case 'a'...'d':
		case 'A'...'D':
			// Armazenamos o que vai mudar numa vari�vel
			aumento = 10; 
			// Informamos qual o aumento ao usu�rio
			printf("Aumento de %i%%.\n", aumento); // O printf interpretaria o % como uma formata��o, ent�o para escrever "10%" usar o % duas vezes
			break;
		
		case 'e':
		case 'E':
			aumento = 15;
			printf("Aumento de %i%%.\n", aumento);
			break;
		
		case 'f'...'g':
		case 'G'...'G':
			aumento = 25;
			printf("Aumento de %i%%.\n", aumento);
			break;
		
		case 'h'...'j':
		case 'H'...'J':
			aumento = 35;
			printf("Aumento de %i%%.\n", aumento);
			break;
			
		case 'k'...'n':
		case 'K'...'N':
			aumento = 50;
			printf("Aumento de %i%%.\n", aumento);
			break;
			
		default:
			printf("N�o houve aumento para essa categoria.\n");
			aumento = 0;
	}
	
	// Calculamos o novo sal�rio e armazenamos seu valor na vari�vel
	/* 	
		Porcentagem de aumento = valor * (porcentagem / 100)
		Note que usamos 100.0 para indicar ao compilador que queremos um n�mero decimal, 
		evitando que ele arredonde o valor e retorne 0
	*/
	novoSalario = salario + (salario * (aumento / 100.0)); 
	
	// Imprimimos o novo sal�rio, utilizando %.2f para indicar que queremos mostrar apenas 2 casas decimais
	printf("\nO novo sal�rio deste funcion�rio ser� R$%.2f\n", novoSalario);
	printf("Houve um aumento de R$%.2f", novoSalario - salario);
	
	return 0;
}
