/*2) O Índice de Massa Corporal (IMC) é um índice que indica se um
adulto está abaixo do peso ideal, ou seja com IMC menor que 20. No
peso ideal o IMC deve estar entre 20 e 25. Se estiver com sobrepeso o
IMC estará entre 25 e 30. Caso contrário estará obeso. A fórmula para
calcular o Índice de Massa Corporal (IMC) é o peso dividido pelo
quadrado da altura. Crie um programa em Pascal que calcule o IMC de
um indivíduo qualquer e diga em qual faixa o indivíduo se encaixa.
Para emitir uma resposta mais completa, peça além das informações
necessárias para o cálculo do IMC, o nome, a idade e o sexo do
indivíduo.*/

// Biblioteca para operações de entrada e saída
#include <stdio.h>
// Biblioteca para operações do sistema (usamos a system("cls"))
#include <stdlib.h>
// Biblioteca para usar acentuação
#include <locale.h>
// Biblioteca para operações com caracteres (usamos a tolower(char))
#include <ctype.h>

int main() {
    char nome[80];
    int idade;
    char sexo;
    float altura, peso, imc;

    // Ler os valores de nome, idade, sexo, altura e peso
    printf("Informe seu nome\n");
    fflush(stdin);
    gets(nome);

    printf("Informe sua idade\n");
    scanf("%i", &idade);

    printf("Informe seu sexo\n");
    fflush(stdin);
    scanf("%c", &sexo);

    printf("Informe sua altura\n");
    scanf("%f", &altura);

    printf("Informe seu peso\n");
    scanf("%f", &peso);

    // Calcular o IMC com base na fórmula passada (peso dividido pelo quadrado da altura)
    imc = peso / (altura * altura);

    // Limpar a tela
    system("cls");

    // Imprimir o relatório
    printf("Nome: %s - %i anos\n", nome, idade);

    printf("Sexo: ");
    // Aqui usamos a função 'tolower' para converter o caractere em minúsculo, evitando ter que fazer duas comparações
    if (tolower(sexo) == 'm') {
        printf("Masculino");
    } else if (tolower(sexo) == 'f') {
        printf("Feminino");
    } else {
        printf("Indeterminado");
    }
    printf("\n");

    // Comparamos o resultado do IMC de acordo com os valores passados
    // Menor que 20: Abaixo do peso
    // Entre 20 e 25: Peso ideal
    // Entre 25 e 30: Sobrepeso
    // Acima de 30: Obeso

    printf("Status de acordo com o IMC: ");
    // Abaixo do peso
    if (imc < 20) {
        printf("Abaixo do peso");
    }
    // Peso ideal
    else if (imc >= 20 && imc <= 25) {
        printf("Peso ideal");
    }
    // Sobrepeso
    else if (imc > 25 && imc <= 30) {
        printf("Sobrepeso");
    }
    // Obeso
    else {
        printf("Obesidade");
    }

    return 0;
}
