/*1) Um usuário da Internet paga uma mensalidade p/ uso de no máximo
60 horas mensais. Solicite ao usuário, o seu nome completo, quantas
horas ele usou no mês, a descrição do mês a que se refere as horas,
bem como, o valor da hora. Quando esse usuário excede o limite de
horas máximas permitidas (60), ele paga um valor de acréscimo (para
efeito de cálculo, usar o acréscimo (por hora excedida) como sendo
valor da hora mais R$ 1,50). Faça um algoritmo para calcular o total
gasto pelo usuário no final do mês e apresente ao usuário para
pagamento.*/

// Biblioteca para operações de entrada e saída
#include <stdio.h>
// Biblioteca para operações do sistema (usamos a system("cls"))
#include <stdlib.h>
// Biblioteca para usar acentuação
#include <locale.h>

int main() {
    // Comando para usar acentuação
    setlocale(LC_ALL, "");

    char nome[120];
    int horas, horasExcedidas;
    float valorHora, acrescimo, totalGasto;

    printf("Insira seu nome completo\n");
    // Limpamos o buffer do teclado para evitar passar direto
    fflush(stdin);
    gets(nome);

    printf("Quantas horas você usou por mês? Digite um número inteiro\n");
    scanf("%i", &horas);

    printf("Qual o valor da hora, em reais?\nR$");
    scanf("%f", &valorHora);

    // Se excedeu o máximo de horas permitidas
    if (horas > 60) {
        // Calcular o total de horas excedidas
        horasExcedidas = horas - 60;
        // valorHora + 1.50 é valor pago por hora com o acréscimo
        acrescimo = horasExcedidas * (valorHora + 1.50);
    } else {
        // Se não excedeu o limite, zerar as variáveis
        horasExcedidas = 0;
        acrescimo = 0;
    }

    // Calcular o total gasto e adicionar o acréscimo
    totalGasto = (horas * valorHora) + acrescimo;

    // Limpar a tela
    system("cls");
    // Imprimir o relatório
    printf("Usuário: %s\n", nome);
    printf("Usou %i horas (%i horas acima do limite)\n", horas, horasExcedidas);
    // R$%0.2f formatará um número com o sinal da moeda e duas casas decimais, ex. R$200,00
    printf("Acréscimo na conta: R$%0.2f\n", acrescimo);
    printf("Total a ser pago: R$%0.2f", totalGasto);

    // Encerramos o programa
    return 0;
}
