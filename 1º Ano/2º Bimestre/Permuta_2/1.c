/* Faça um algoritmo que receba as vendas semanais de um mês (4 semanas) de 5 vendedores de uma
loja e armazene essas vendas em uma matriz. (matriz 5 x 4).
Calcule e imprima:
Total de vendas do mês (4 semanas) de cada vendedor;
Total de vendas de cada semana (todos os vendedores juntos);
Total de vendas do mês (todos os vendedores juntos). */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int v; // variável usada para percorrer as linhas do vetor, onde cada linha representa um vendedor diferente
    int s; // variável usada para percorrer as colunas do vetor, onde cada colunas representa uma semana diferente
    int vendas[5][4]; // matriz 5x4 para armazenar as vendas, onde cada linha remete a um vendedor e cada coluna remete a uma semana
    int somaDoVendedor[5] = {0, 0, 0, 0, 0}; // vetor usado para armazenar a soma das vendas por vendedor, com todos os valores inicializados em 0
    int somaDaSemana[4] = {0, 0, 0, 0}; // vetor usado para armazenar a soma das vendas por semana, com todos os valores inicializados em 0
    int somaGeral = 0; // variável usada para armazenar a soma de todos os vendedores juntos

    // Estrutura que percorrerá todas as linhas/vendedores da matriz
        for (v = 0; v < 5; v++) {
        // Aqui usamos v+1 pois o nosso índice começa em 0, mas não podemos falar "0º vendedor"
        printf("Vendas do(a) %iº vendedor(a):\n", v+1);
        for (s = 0; s < 4; s++) {
            // Mesma coisa com o s+1, não podemos falar "semana 0"
            printf("Informe a quantidade de vendas da semana %i: ", s+1);
            // Ler a quantidade de vendas
            scanf("%i", &vendas[v][s]);

            // Acrescentar o valor lido ao total de vendas daquele vendedor
            somaDoVendedor[v] += vendas[v][s];
            // Acrescentar o valor lido ao total de vendas daquela semana
            somaDaSemana[s] += vendas[v][s];
            // Acrescentar o valor lido ao total de vendas do mês
            somaGeral += vendas[v][s];
        }
        // Pular uma linha a cada vendedor lido para melhorar o visual do programa
        printf("\n");
    }

    printf("\nRelatórios:\n");

    // Total por vendedor
    printf("Total de vendas do mês (4 semanas) de cada vendedor:\n");
    // Percorrer todos os elementos da somaDoVendedor e mostrar eles na tela
    for (v = 0; v < 5; v++) {
        printf("Vendedor %i: %i\n", v+1, somaDoVendedor[v]);
    }

    // Total por semana
    printf("\nTotal de vendas de cada semana (todos os vendedores juntos):\n");
    // Percorrer todos os elementos da somaDoVendedor e mostrar eles na tela
    for (s = 0; s < 4; s++) {
        printf("Semana %i: %i\n", s+1, somaDaSemana[s]);
    }

    // Total no mês
    printf("\nTotal de vendas do mês (todos os vendedores juntos):\n%i", somaGeral);

    // Terminar a execução do programa
    return 0;
}
