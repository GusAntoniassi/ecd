/* Achar a somatória de cada uma das linhas de uma matriz (7x5), listando no final a somatória de
todas as linhas */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int li, col; // variáveis de linha e coluna utilizados na estrutura de repetição
    int matriz[7][5]; // variável da matriz 7x5 que irá armazenar os números
    int somaLinha = 0, somaGeral = 0; // variáveis para armazenar a soma de cada linha e a soma de todas as linhas

    // Estrutura para repetir por todas as linhas
    for (li = 0; li < 7; li++) {
        // Zerar a soma das linhas cada vez que começarmos a ler uma linha nova
        somaLinha = 0;

        // Pedir ao usuário os números da linha
        printf("Entre com os números da linha %i:\n", li);

        // Estrutura para repetir por cada coluna dentro da linha
        for (col = 0; col < 5; col++) {
            // Ler o número informado pelo usuário
            scanf("%i", &matriz[li][col]);
            // Acrescentar esse número na soma da linha atual
            somaLinha += matriz[li][col];
        }

        // Após ler todos os números da linha, apresentamos a somatória dos valores
        printf("Somatório: %i\n\n", somaLinha);
        // E acrescentamos essa somatória na somatória geral da matriz
        somaGeral += somaLinha;
    }

    // Após ler todas as linhas, apresentamos a somatória geral da matriz
    printf("A soma de todas as linhas é: %i", somaGeral);

    // Terminar a execução do programa
    return 0;
}

