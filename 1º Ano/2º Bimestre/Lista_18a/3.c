/* Dada uma matriz de (7x7), separar os elementos da diagonal principal (esquerda para a direita, de
cima para baixo) e armazene num vetor. No final liste a somatória dos valores inseridos no vetor. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int li, col; // variáveis de linha e coluna utilizados nas estruturas de repetição
    int matriz[7][7]; // matriz 7x7 onde serão armazenados os elementos
    int diagonal[7]; // vetor onde serão armazenados os elementos da diagonal principal da matriz
    int somaDiagonal = 0; // variável de somatória dos elementos da diagonal principal

    // Estrutura que irá percorrer todas as linhas da matriz
    for (li = 0; li < 7; li++) {
        printf("Entre com 7 valores:\n");
        // Estrutura que irá percorrer todas as colunas da matriz
        for (col = 0; col < 7; col++) {
            // Ler um valor entrado pelo usuário
            scanf("%i", &matriz[li][col]);

            // Um elemento está na diagonal principal quando o índice de linhas é igual o de colunas.
            // ex [1][1], [2][2], [3][3], etc.
            if (li == col) {
                // Armazenar o valor entrado no vetor da diagonal principal
                diagonal[li] = matriz[li][col];
                // Adicionar o valor entrado na somatória da diagonal
                somaDiagonal += diagonal[li];
            }
        }
    }

    // Mostrar a somatória para o usuário
    printf("A soma dos valores inseridos na diagonal é: %i", somaDiagonal);

    // Terminar a execução do programa
    return 0;
}
