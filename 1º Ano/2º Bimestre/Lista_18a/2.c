/* Achar a somatória de cada uma das colunas de uma matriz (5x10), listando no final a somatória de
todas as colunas. Crie uma linha a mais e armazene dentro desta última linha a soma das colunas */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int li, col; // variáveis de linha e coluna utilizadas nas estruturas de repetição
    int matriz[6][10]; // matriz 5x10, aqui declarada como 6x10 pois a última armazenará a soma
    int somaCol = 0; // variável para armazenar a somatória da coluna

    // Estrutura que passará por todas as colunas da matriz
    for (col = 0; col < 10; col++) {
        printf("Entre com 5 valores:\n");
        // Estrutura que passará por todas as linhas da matriz
        // Aqui iremos da linha 0 até a 4, pois a última linha (linha 5) receberá a soma das colunas, e não
        // um valor entrado pelo usuário como as outras linhas
        for (li = 0; li < 5; li++) {
            // Ler o valor entrado pelo usuário
            scanf("%i", &matriz[li][col]);
            // Acrescentar o valor lido à somatória daquela coluna
            somaCol += matriz[li][col];
        }
        // Armazenar a somatória da coluna na última linha do vetor
        matriz[5][col] = somaCol;
        // Zerar a variável da somatória após salvarmos o valor
        somaCol = 0;
    }

    // Estrutura de impressão, que mostrará todos os valores lidos na tela
    for (li = 0; li < 6; li++) {
        // Se estivermos lendo a última linha (índice 5), então imprimimos um separador para diferenciar a soma
        if (li == 5) {
            printf("\n---------------------------------------------------------------------\n");
        // Se for qualquer outra linha, apenas pulamos uma linha
        } else {
            printf("\n");
        }

        // Repetir por todas as colunas
        for (col = 0; col < 10; col++) {
            // Imprimir o valor seguido de um espaço variável (\t)
            printf("%i\t", matriz[li][col]);
        }
    }

    // Terminar a execução do programa
    return 0;
}
