/* Fazer um algoritmo que mostre no vídeo os números pares compreendidos numa faixa que
o usuário irá determinar. Após o usuário determinar o valor inicial e final da faixa
desejada, fazer o bloqueio de valores inválidos, ou seja, se o numero inicial for igual ou
maior que o valor final, pedir valores novamente. Utilize para fazer esse teste uma outra
estrutura de repetição. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont; // contador que será utilizado na estrutura de repetição
    int inicio, fim; // variáveis utilizadas para armazenar a faixa que o usuário irá informar

    // Estrutura que irá repetir até o usuário entrar com uma faixa válida
    do {
        // Pedir e ler o início da faixa
        printf("Informe o valor inicial da faixa:\n");
        scanf("%i", &inicio);

        // Pedir e ler o fim da faixa
        printf("Informe o valor final da faixa:\n");
        scanf("%i", &fim);

        // Mostrar uma mensagem de erro se a faixa for inválida
        if (fim <= inicio) {
            printf("O valor final não pode ser menor ou igual ao inicial! Tente novamente.\n\n");
        }
    } while (fim <= inicio);

    printf("\n");

    // Iniciar o contador no começo da faixa
    cont = inicio;
    // Repetir até o contador chegar no fim da faixa
    while (cont <= fim) {
        // Se o número for par, mostrar ele na tela
        if (cont % 2 == 0) {
            printf("%i\n", cont);
        }
        // Incrementar o contador
        cont++;
    }

    // Terminar a execução do programa
    return 0;
}

