/* Crie um algoritmo que leia 50 números inteiros positivos e imprima o maior. Fazer o teste
para verificar se o numero digitado é positivo, se não for, tem que pedir novo número.
Utilize para fazer esse teste uma outra estrutura de repetição. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont = 0; // contador utilizado na estrutura de repetição
    int num; // variável para armazenar o número que o usuário irá digitar
    int maior = 0; // variável para armazenar o maior número digitado

    // Estrutura que irá repetir até lermos 50 números
    while (cont < 50) {
        printf("Informe um número:\n");

        // Repetir enquanto o número informado for negativo
        do {
            scanf("%i", &num);

            // Mostrar uma mensagem de erro se o número for negativo
            if (num < 0) {
                printf("O número não pode ser negativo! Tente novamente:\n");
            }
        } while (num < 0);

        // Se o número lido for o primeiro OU for maior que o maior número que já lemos
        if (cont == 0 || num > maior) {
            // Ele passa a ser o maior número que já lemos
            maior = num;
        }

        // Incrementar o nosso contador
        cont++;
    }

    // Mostrar o maior número ao usuário
    printf("O maior número digitado é: %i", maior);

    // Terminar a execução do programa
    return 0;
}
