/* Crie um algoritmo que leia 3 números inteiros (utilize três variáveis para fazer as leituras).
Faça os bloqueios para valores negativos e valores iguais. Utilize para fazer esses testes
uma outra estrutura de repetição. No final imprima o maior valor */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int num1, num2, num3;

    printf("Informe o primeiro número:\n");
    // Repetir enquanto o usuário digitar um número negativo
    do {
        scanf("%i", &num1);

        // Mostrar uma mensagem de erro se o número for negativo
        if (num1 < 0) {
            printf("O número não pode ser negativo! Tente novamente:\n");
        }
    } while (num1 < 0);

    printf("Informe o segundo número:\n");
    // Repetir enquanto o usuário digitar um número negativo ou igual ao primeiro
    do {
        scanf("%i", &num2);

        // Mostrar uma mensagem de erro se o número for negativo
        if (num2 < 0) {
            printf("O número não pode ser negativo! Tente novamente:\n");
        // Mostrar uma mensagem de erro se o número for igual ao primeiro
        } else if (num2 == num1) {
            printf("O número não pode ser igual ao primeiro! Tente novamente:\n");
        }
    } while (num2 < 0 || num2 == num1);

    // Pedir e ler o terceiro número
    printf("Informe o terceiro número:\n");
    // Repetir enquanto o usuário digitar um número negativo ou igual ao primeiro ou segundo
    do {
        scanf("%i", &num3);

        // Mostrar uma mensagem de erro se o número for negativo
        if (num3 < 0) {
            printf("O número não pode ser negativo! Tente novamente:\n");
        // Mostrar uma mensagem de erro se o número for igual ao primeiro
        } else if (num3 == num1) {
            printf("O número não pode ser igual ao primeiro! Tente novamente:\n");
        // Mostrar uma mensagem de erro se o número for igual ao segundo
        } else if (num3 == num2) {
            printf("O número não pode ser igual ao segundo! Tente novamente:\n");
        }
    } while (num3 < 0 || num3 == num1 || num3 == num2);

    // Se o primeiro número for maior que o segundo e maior que o terceiro, ele é o maior de todos
    if (num1 > num2 && num1 > num3) {
        printf("O primeiro número (%i) é o maior.", num1);
    // Se o primeiro número não for o maior de todos, e o segundo for maior que o terceiro
    } else if (num2 > num3) {
        printf("O segundo número (%i) é o maior.", num2);
    // Se nem o primeiro nem o segundo número forem o maior
    } else {
        printf("O terceiro número (%i) é o maior.", num3);
    }

    // Terminar a execução do programa
    return 0;
}

