/* Leia 20 valores reais e escreva o seu somatório */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int contador = 0; // contador que será utilizado na estrutura de repetição
    float numInformado = 0; // número que o usuário irá informar
    float somatorio = 0; // somatório dos números informados pelo usuário

    // Laço que repetirá até que o contador seja maior ou igual a cinco
    do {
        // Pedir e ler o número para o usuário
        printf("Informe um número:\n");
        scanf("%f", &numInformado);

        // Acrescentar o número informado ao nosso somatório
        somatorio += numInformado;

        // Incrementar o contador
        contador++;
    } while (contador < 20);

    // Mostrar o resultado ao usuário
    printf("O somatório dos números informados é: %0.2f", somatorio);

    // Terminar a execução do programa
    return 0;
}
