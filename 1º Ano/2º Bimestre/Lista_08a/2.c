/* Fa�a um algoritmo DIN�MICO para ler e escrever o NOME de um numero indefinido de
pessoas. */

#include <stdio.h>
#include <locale.h>
#include <conio.h>

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declara��o de vari�veis
    char nomePessoa[80]; // vari�vel para armazenar o nome digitado
    char sair = 'n'; // vari�vel para saber se o usu�rio quer encerrar a execu��o (iniciada como "n�o")

    // La�o que vai repetir enquanto o usu�rio n�o digitar 's' ou 'S' quando perguntado se deseja sair
    do {
        // Ler o nome da pessoa
        printf("Insira o nome da pessoa: \n");
        fflush(stdin);
        gets(nomePessoa);

        // Mostrar qual o nome digitado na tela
        printf("O seu nome � %s\n", nomePessoa);

        // Perguntar se o usu�rio quer sair
        printf("Deseja sair? (S ou N)\n");
        fflush(stdin);
        sair = getche();
    } while (sair != 's' && sair != 'S');

    return 0;
}
