/* Faça um algoritmo para ler a base e a altura de 5 triângulos e imprima a de área cada um.
 (area := (base * altura) /2). */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int contador = 0; // contador que será utilizado na estrutura de repetição
    float base, altura, area; // variáveis utilizadas no cálculo da área do triângulo

    // Laço que repetirá até que o contador seja maior ou igual a cinco
    while (contador < 5) {
        // Perguntar e ler a base do triângulo
        printf("Informe a base do triângulo:\n");
        scanf("%f", &base);

        // Perguntar e ler a altura do triângulo
        printf("Informe a altura do triângulo:\n");
        scanf("%f", &altura);

        // Calcular a área com base na fórmula passada
        area = (base * altura) / 2;

        // Imprimir o resultado do cálculo
        printf("\nA área do triângulo é: %0.2f\n", area);

        // Imprimir uma linha de separação, apenas para fins estéticos
        printf("----------------------------------\n");

        // Incrementar o contador
        contador++;
    }

    // Terminar a execução do programa
    return 0;
}
