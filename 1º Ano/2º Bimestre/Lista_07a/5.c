/* Fazer um algoritmo que imprima 10 números PARES fornecidos pelo usuário

   (Esse exercício está aberto à interpretação. Aqui vamos implementar a
   seguinte lógica: O programa vai perguntar um número pro usuário, e só
   vai parar quando o usuário digitar 10 números pares)                    */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int contPares = 0; // contador de números pares digitados pelo usuário
    int numInformado = 0; // variável que vai armazenar o número informado pelo usuário

    // Repetir até o usuário digitar um total de 10 números pares
    while (contPares < 10) {
        // Ler o número informado
        printf("Insira um número:\n");
        scanf("%i", &numInformado);

        // Se dividirmos o número por 2 e o resto da divisão for 0, significa que o número é par
        if (numInformado % 2 == 0) {
            printf("O número %i é par!\n", numInformado);
            contPares++;
        }

        printf("\n");
    }

    // Terminar a execução do programa
    return 0;
}
