/* Fazer um algoritmo que imprima os números ÍMPARES entre 0 e 10. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int contador = 0; // contador que será utilizado na estrutura de repetição

    // Laço que repetirá até que o contador seja maior ou igual a dez
    while (contador < 10) {
        // Se dividirmos o contador por 2 e o resto da divisão for 1, significa que o número é ímpar
        if (contador % 2 == 1) {
            // Mostrar o número ímpar na tela, seguido de uma quebra de linha
            printf("%i\n", contador);
        }

        // Incrementar o contador
        contador++;
    }

    // Terminar a execução do programa
    return 0;
}
