/*Faça um algoritmo ESTÁTICO para ler e escrever o NOME de 20 pessoas*/

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    char nomePessoa[80]; // variável para armazenar o nome digitado
    int pessoasLidas = 0; // contador para saber quantas pessoas já lemos

    // Laço que vai repetir até o nosso contador chegar a 20
    while (pessoasLidas < 20) {
        // Ler o nome da pessoa
        printf("Insira o nome: \n");
        fflush(stdin); // limpar o buffer do teclado
        gets(nomePessoa);

        // Incrementar o contador de pessoas lidas para indicar que mais uma pessoa foi lida
        // (mesma coisa que usar "pessoasLidas = pessoasLidas + 1")
        pessoasLidas++;

        // Mostrar qual o nome digitado na tela
        printf("O seu nome é %s\n\n", nomePessoa);
    }

    // Terminar a execução do programa
    return 0;
}
