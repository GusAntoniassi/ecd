/* Faça um algoritmo que carregue uma matriz 2x2 com números reais, calcule e imprima a soma dos
elementos que não fazem parte da diagonal principal (esquerda para a direita, de cima para baixo) */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int li, col; // variáveis de linha e coluna utilizadas nas estruturas de repetição
    float matriz[2][2]; // matriz 2x2 usada para armazenar os valores
    float soma = 0; // variável para armazenar a soma dos elementos que não fazem parte da diagonal principal

    // Estrutura que percorrerá as duas linhas do vetor
    for (li = 0; li < 2; li++) {
        printf("Entre com 2 valores:\n");
        // Estrutura que percorrerá as duas colunas do vetor
        for (col = 0; col < 2; col++) {
            // Ler o valor que o usuário entrar
            scanf("%f", &matriz[li][col]);

            // Se a linha e a coluna são dois números diferentes, isso significa que não estamos na diagonal principal
            if (li != col) {
                // Adicionar o valor lido à soma dos valores fora da diagonal principal
                soma += matriz[li][col];
            }
        }
    }

    // Mostrar a soma ao usuário
    printf("A soma dos elementos fora da diagonal principal é: %0.2f", soma);

    // Terminar a execução do programa
    return 0;
}
