/* Faça um algoritmo que carregue uma matriz 3x3 e no final imprima o maior elemento dessa matriz */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int li, col; // variáveis de linha e coluna utilizadas nas estruturas de repetição
    int matriz[3][3]; // matriz 3x3 usada para armazenar os valores
    int maior = 0; // variável usada para armazenar o maior elemento inserido na matriz

    // Estrutura que percorrerá as 3 linhas da matriz
    for (li = 0; li < 3; li++) {
        printf("Entre com 3 valores:\n");
        // Estrutura que percorrerá as 3 colunas da matriz
        for (col = 0; col < 3; col++) {
            // Ler o valor que o usuário informou
            scanf("%i", &matriz[li][col]);

            // Se o número lido foi o primeiro (na linha 0, coluna 0)
            // OU o número lido é maior que o maior número que temos armazenado
            if ((li == 0 && col == 0) || matriz[li][col] > maior) {
                // Armazenar o número lido como o maior número até agora
                maior = matriz[li][col];
            }
        }
    }

    // Mostrar o maior número ao usuário
    printf("O maior elemento inserido é: %i", maior);

    // Terminar a execução do programa
    return 0;
}
