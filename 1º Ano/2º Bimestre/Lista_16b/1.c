/* Tendo uma lista contendo a idade de 10 pessoas, apresente quantos por cento são maiores de idade */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont; // contador usado na estrutura de repetição
    int idades[10]; // vetor para armazenar a idade das 10 pessoas
    int qtdeMaiores = 0; // variável para armazenar quantas pessoas são maiores de idade
    float porcMaiores = 0; // variável para calcular quantos por cento são maiores de idade

    // Estrutura de repetição que lerá a idade das 10 pessoas, e calculará quantos são maiores de idade
    for (cont = 0; cont < 10; cont++) {
        // Pedir e ler a idade
        printf("Informe a idade:\n");
        scanf("%i", &idades[cont]);

        // Se a idade informada for maior que 18, incrementar a quantidade de maiores de idade em 1
        if (idades[cont] > 18) {
            qtdeMaiores++;
        }
    }

    // Cálculo da porcentagem: porcentagem = (quantidade de maiores * 100) / total de pessoas
    porcMaiores = qtdeMaiores * 10.0;
    // Mostrar o resultado do cálculo ao usuário
    printf("Da lista de 10 pessoas, %0.0f% são maiores de idade", porcMaiores);

    // Terminar a execução do programa
    return 0;
}

