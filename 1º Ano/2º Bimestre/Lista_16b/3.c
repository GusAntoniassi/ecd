/* Dada uma lista de 10 números, apresentar a diferença entre o maior e o menor número. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont; // contador que será utilizado na estrutura de repetição
    int numeros[10]; // vetor que armazenará os 10 números
    int maior = 0, menor = 0; // variáveis para armazenar o maior e menor valor da lista

    // Estruturá que lerá os 10 números e calculará o maior e o menor
    for (cont = 0; cont < 10; cont++) {
        // Pedir e ler um número ao usuário
        printf("Informe um número:\n");
        scanf("%i", &numeros[cont]);

        // Se o número que lemos é o primeiro, então ele vai ser tanto o maior número que já temos quanto o menor
        if (cont == 0) {
            // Armazenar o primeiro número nas variáveis de maior e menor
            maior = numeros[cont];
            menor = numeros[cont];
        // Se o número que lemos é maior que o maior valor que temos armazenado, então ele será o maior número até agora
        } else if (numeros[cont] > maior) {
            // Armazenar o número lido como maior número até agora
            maior = numeros[cont];
        // Se o número que lemos é menor que o menor valor que temos armazenado, então ele será o menor número até agora
        } else if (numeros[cont] < menor) {
            // Armazenar o número lido como menor número até agora
            menor = numeros[cont];
        }
    }

    // Mostrar os números ao usuário
    printf("Maior número: %i, menor número: %i, diferença: %i", maior, menor, maior-menor);

    // Terminar a execução do programa
    return 0;
}
