/* Solicitando ao usuário 19 números, apresente a média dos que foram informados em posições
ímpares. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont; // contador que será utilizado na estrutura de repetição
    int numeros[19]; // vetor que armazenará os 40 números
    int somaPosImpares = 0; // variável para armazenar a soma dos números armazenados em posições ímpares
    float mediaPosImpares = 0; // variável para armazenar a média dos números armazenados em posições ímpares

    // Estrutura de repetição que lerá os 40 números e fará o somatório daqueles armazenados em posições ímpares
    for (cont = 0; cont < 19; cont++) {
        // Pedir e ler um número ao usuário
        printf("Informe um número:\n");
        scanf("%i", &numeros[cont]);

        // Se o nosso contador for ímpar, significa que o número está sendo armazenado em uma posição ímpar
        if (cont % 2 == 1) {
            // Adicionar o número à variável da somatória
            somaPosImpares += numeros[cont];
        }
    }

    // Fazer o cálculo da média dos números (como temos 40 números, 20 terão posições ímpares e 20 terão posições ímpares)
    mediaPosImpares = somaPosImpares / 9.0;
    // Mostrar a média ao usuário
    printf("A média dos números informados em posições ímpares é: %0.2f", mediaPosImpares);

    // Terminar a execução do programa
    return 0;
}

