/* Tendo uma lista contendo a idade de 12 pessoas, apresente quantos por cento são menores de idade. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont; // contador usado na estrutura de repetição
    int idades[12]; // vetor para armazenar a idade das 12 pessoas
    int qtdeMenores = 0; // variável para armazenar quantas pessoas são menores de idade
    float porcMenores = 0; // variável para calcular quantos por cento são menores de idade

    // Estrutura de repetição que lerá a idade das 12 pessoas, e calculará quantos são menores de idade
    for (cont = 0; cont < 12; cont++) {
        // Pedir e ler a idade
        printf("Informe a idade:\n");
        scanf("%i", &idades[cont]);

        // Se a idade informada for maior que 18, incrementar a quantidade de maiores de idade em 1
        if (idades[cont] < 18) {
            qtdeMenores++;
        }
    }

    // Cálculo da porcentagem: porcentagem = (quantidade de maiores * 100) / total de pessoas
    porcMenores = (qtdeMenores * 100.0) / 12.0;
    // Mostrar o resultado do cálculo ao usuário
    printf("Da lista de 12 pessoas, %0.0f% são menores de idade", porcMenores);

    // Terminar a execução do programa
    return 0;
}
