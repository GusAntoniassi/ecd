/* Solicitando ao usuário 40 números, apresente a média dos que foram informados em posições pares */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont; // contador que será utilizado na estrutura de repetição
    int numeros[10]; // vetor que armazenará os 40 números
    int somaPosPares = 0; // variável para armazenar a soma dos números armazenados em posições pares
    float mediaPosPares = 0; // variável para armazenar a média dos números armazenados em posições pares

    // Estrutura de repetição que lerá os 40 números e fará o somatório daqueles armazenados em posições pares
    for (cont = 0; cont < 10; cont++) {
        // Pedir e ler um número ao usuário
        printf("Informe um número:\n");
        scanf("%i", &numeros[cont]);

        // Se o nosso contador for par, significa que o número está sendo armazenado em uma posição par
        if (cont % 2 == 0) {
            // Adicionar o número à variável da somatória
            somaPosPares += numeros[cont];
        }
    }

    // Fazer o cálculo da média dos números (como temos 40 números, 20 terão posições pares e 20 terão posições ímpares)
    mediaPosPares = somaPosPares / 20.0;
    // Mostrar a média ao usuário
    printf("A média dos números informados em posições pares é: %0.2f", mediaPosPares);

    // Terminar a execução do programa
    return 0;
}
