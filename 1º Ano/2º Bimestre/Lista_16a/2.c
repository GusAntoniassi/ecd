/* Leia valores para três vetores com capacidade de armazenar 10 números inteiro (cada um) e
armazene a soma deles num quarto vetor, imprimindo a soma parcial (soma dos valores lidos nos três
vetores) e no final mostre a soma total dos valores acumulados no quarto vetor. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int vet1[10], vet2[10], vet3[10], soma[10]; // vetores utilizados para armazenar os valores informados
    int somaTotal = 0; // soma total dos valores inseridos nos três vetores
    int cont = 0; // contador para a estrutura de repetição

    // Estrutura de repetição para ler os 10 números nos 3 vetores
    for (cont = 0; cont < 10; cont++) {
        // Perguntar e ler o valor do primeiro vetor
        printf("Informe um valor para o primeiro vetor:\n");
        scanf("%i", &vet1[cont]);

        // Perguntar e ler o valor do segundo vetor
        printf("Informe um valor para o segundo vetor:\n");
        scanf("%i", &vet2[cont]);

        // Perguntar e ler o valor do terceiro vetor
        printf("Informe um valor para o terceiro vetor:\n");
        scanf("%i", &vet3[cont]);

        // Armazenar a soma dos três vetores no vetor de soma
        soma[cont] = vet1[cont] + vet2[cont] + vet3[cont];

        // Mostrar a soma parcial ao usuário
        printf("Soma parcial: %i\n\n", soma[cont]);

        // Armazenar a soma parcial no somatório geral
        somaTotal += soma[cont];
    }
    printf("------------------------------------\n");
    // Mostrar a soma geral ao usuário
    printf("Soma total: %i", somaTotal);

    // Terminar a execução do programa
    return 0;
}
