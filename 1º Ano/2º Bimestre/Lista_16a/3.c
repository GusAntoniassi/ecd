/* Solicitando ao usuário 6 valores, os quais devem ser armazenados num único vetor, apresente o
maior valor e a posição em que o mesmo foi informado. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont = 0; // contador para a estrutura de repetição
    int vetor[6]; // vetor para armazenar os 6 valores
    int maior = 0; // variável para armazenar o maior número informado

    // Estrutura de repetição para ler os 6 valores
    for (cont = 0; cont < 6; cont++) {
        // Pedir e ler o valor para o usuário
        printf("Entre com um valor:\n");
        scanf("%i", &vetor[cont]);

        // Se o valor informado for maior que o maior número que nós temos armazenado,
        // OU se ele é o primeiro valor a ser informado, significa que ele é o maior valor
        // que nós temos até agora
        if (vetor[cont] > maior || cont == 0) {
            // Armazenar o valor informado na variável do maior valor
            maior = vetor[cont];
        }
    }

    // Mostrar ao usuário qual foi o maior valor informado
    printf("O maior valor informado foi: %i", maior);

    // Terminar a execução do programa
    return 0;
}
