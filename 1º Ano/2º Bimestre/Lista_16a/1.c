/* Num único vetor de 8 posições, entre com os valores e ao final mostre a somatória dos valores
armazenados nas respectivas posições. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int vetor[8]; // vetor de 8 posições
    int cont; // contador para a estrutura for
    int somatoria = 0; // somatória dos valores armazenados

    // Estrutura de repetição para inserir os 8 valores no vetor
    for (cont = 0; cont < 8; cont++) {
        // Perguntar e ler o valor ao usuário
        printf("Entre com um valor:\n");
        scanf("%i", &vetor[cont]);

        // Armazenar o valor lido na somatória dos valores armazenados
        somatoria += vetor[cont];
    }

    // Mostrar a somatória para o usuário
    printf("A somatória é: %i", somatoria);

    // Terminar a execução do programa
    return 0;
}
