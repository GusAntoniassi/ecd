/* Solicitando ao usuário 20 valores maior que zero, os quais devem ser armazenados num único vetor,
apresente o maior, o menor e suas respectivas posições em que os mesmos foram informados. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");
 
    // Declaração de variáveis
    int cont; // contador para a estrutura de repetição
    int vetor[20]; // vetor para armazenar os 20 valores
    int maior = 0, menor = 0; // variáveis para armazenar o maior e o menor número
    int posicaoMaior, posicaoMenor; // variáveis para armazenar o índice (posição) do maior e menor número

    // Estrutura de repetição para ler os 20 números
    for (cont = 0; cont < 20; cont++) {
        // Pedir o número ao usuário
        printf("Informe um valor:\n");

        // Estrutura que vai pedir o número até que o usuário informe um valor positivo
        do {
            scanf("%i", &vetor[cont]);
            if (vetor[cont] < 0) {
                printf("Valor negativo! Tente novamente:\n");
            }
        } while (vetor[cont] < 0);

        // Se o número que lemos é o primeiro, então ele vai ser tanto o maior número que já temos quanto o menor
        if (cont == 0) {
            // Armazenar o primeiro número nas variáveis de maior e menor
            maior = vetor[cont];
            menor = vetor[cont];

            // Armazenar a posição do primeiro número nas variáveis de maior e menor posição
            posicaoMaior = cont;
            posicaoMenor = cont;

        // Se o número que lemos é maior que o maior valor que temos armazenado, então ele será o maior número até agora
        } else if (vetor[cont] > maior) {
            // Armazenar o número e a posição dele nas variáveis de maior número
            maior = vetor[cont];
            posicaoMaior = cont;

        // Se o número que lemos é menor que o menor valor que temos armazenado, então ele será o menor número até agora
        } else if (vetor[cont] < menor) {
            // Armazenar o número e a posição dele nas variáveis de menor número
            menor = vetor[cont];
            posicaoMenor = cont;
        }
    }

    // Mostrar o resultado ao usuário
    printf("O maior valor é %i, armazenado na posição %i\n", maior, posicaoMaior);
    printf("O menor valor é %i, armazenado na posição %i\n", menor, posicaoMenor);

    // Terminar a execução do programa
    return 0;
}
