/*Elabore um algoritmo onde na padaria SIPÃO vende uma certa quantidade de pães e
uma certa quantidade de pedaços de bolo a cada dia. Cada pão custa R$ 0,29 e
cada pedaço de bolo custa R$ 2,17. No final do expediente, o gerente quer saber:
quanto arrecadou com as vendas dos pães e bolos (juntos), e quanto deve guardar
numa conta de poupança (deve guardar 10% do total arrecadado). Você foi
contratado para fazer os cálculos para o gerente. Com base nestes fatos, faça um
algoritmo para ler as quantidades de pães e pedaços de bolo vendidos ao final do
expediente, e depois calcule e apresente os dados solicitados (quanto arrecadou e
quando deve colocar na poupança).*/

#include <stdio.h>
#include <locale.h>

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Constantes de preço
    float precoPao = 0.29;
    float precoBolo = 2.17;

    // Quantidade de pães e bolos vendidos
    int paesVendidos;
    int bolosVendidos;

    // Valores resultados do processamento
    float valorArrecadado;
    float valorAPoupar;

    // Ler o tanto de pães que foram vendidos
    printf("Informe a quantidade de pães vendidos\n");
    scanf("%i", &paesVendidos);

    // Ler o tanto de bolos que foram vendidos
    printf("Informe a quantidade de bolos vendidos\n");
    scanf("%i", &bolosVendidos);

    // Multiplicar a quantidade pelo preço de cada produto, e armazenar a soma na variável
    valorArrecadado = (paesVendidos * precoPao) + (bolosVendidos * precoBolo);
    // Calcular 10% do valor arrecadado
    valorAPoupar = valorArrecadado * (10.0 / 100);

    // Mostrar o resultado das vendas
    printf("O valor arrecadado pela venda de pães foi R$%0.2f\n", paesVendidos * precoPao);
    printf("O valor arrecadado pela venda de bolos foi R$%0.2f\n\n", bolosVendidos * precoBolo);

    // Mostrar o resultado do processamento
    printf("O total arrecadado pelas vendas foi R$%0.2f\n", valorArrecadado);
    printf("O valor a ser colocado na poupança é R$%0.2f\n", valorAPoupar);

    return 0;
}
