/*Elabore um algoritmo onde o usuário fará a leitura de um número (inteiro) e em
seguida, verifique se o número é neutro (igual a zero), positivo ou negativo. Caso o
número seja positivo, apresente o dobro do número. Caso o número seja negativo
apresente a metade deste número. Caso o valor informado seja igual a zero, apenas
emitir uma mensagem dizendo “número neutro – Fim de execução”.*/

#include <stdio.h>
#include <locale.h>

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    int num;

    // Entrada do valor
    printf("Entre com um número\n");
    scanf("%i", &num);

    // Se número for neutro - ou seja, igual a zero
    if (num == 0) {
        printf("Número neutro - fim de execução");
    // Se o número for maior que zero, ele é positivo
    } else if (num > 0){
        printf("O dobro do número é: %i", num * 2);
    // Se não for zero e nem positivo, ele é negativo
    } else {
        printf("A metade do número é: %i", num / 2);
    }

    return 0;
}
