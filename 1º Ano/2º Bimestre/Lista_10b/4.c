/* Entre com o nome, nota1 e nota2 de um numero de alunos determinado pelo usuário. Após
a entrada liste o nome, a nota1 a nota2 e a média desse aluno. Ao final liste a media da turma. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont; // contador que irá de 0 até a quantidade informada pelo usuário
    int qtdeAlunos = 0; // quantidade informada pelo usuário
    char nome[80]; // variável para armazenar o nome do aluno
    float nota1, nota2, media; // variáveis para armazenar as notas e média do aluno
    float somatorioTurma = 0, mediaTurma = 0; // variáveis para armazenar o somatório e média da turma

    // Perguntar e ler a quantidade de alunos que o usuário vai cadastrar
    printf("Quantos alunos você deseja cadastrar?\n");
    scanf("%i", &qtdeAlunos);
    printf("\n");

    // Laço que vai de 0 até a quantidade de alunos que o usuário entrou
    for (cont = 0; cont < qtdeAlunos; cont++) {
        // Perguntar e ler o nome do aluno
        printf("Informe o nome:\n");
        gets(nome);

        // Perguntar e ler a primeira nota
        printf("Informe a nota 1:\n");
        scanf("%f", &nota1);

        // Perguntar e ler a segunda nota
        printf("Informe a nota 2:\n");
        scanf("%f", &nota2);

        // Calcular a média e armazenar na variável
        media = (nota1 + nota2) / 2;

        // Imprimir um relatório do aluno cadastrado
        printf("-------------------------------\n");
        printf("Aluno: %s\n", nome);
        printf("Nota 1: %0.2f - Nota 2: %0.2f\n", nota1, nota2);
        printf("Média: %0.2f\n", media);
        printf("-------------------------------\n");

        // Acrescentar a média calculada no somatório das médias da turma
        somatorioTurma += media;
    }

    // Calcular a média total da turma
    mediaTurma = somatorioTurma / qtdeAlunos;
    // Mostrar o valor para o usuário
    printf("A média da turma é: %0.2f", mediaTurma);

    // Terminar a execução do programa
    return 0;
}
