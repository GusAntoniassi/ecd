/*  Entre com 10 números positivos (bloquear os números negativos com estrutura de repetição
Do...While) e imprima a metade de cada um deles. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont = 0; // contador do laço que irá até 10
    int numInformado = 0; // número que o usuário irá digitar

    // Laço que repetirá 10 vezes
    for (cont = 0; cont < 10; cont++) {
        // Pedir pro usuário entrar com um número
        printf("Entre com um número positivo:\n");

        // Laço que repetirá até que o usuário entre com um número positivo
        do {
            // Ler o número do usuário
            scanf("%i", &numInformado);

            // Condicional para mostrar mensagem de erro ao usuário se ele digitar um número negativo
            if (numInformado < 0) {
                printf("Erro: Número negativo informado! Tente novamente:\n");
            }
        } while (numInformado < 0);

        // Se chegou aqui, significa que o usuário finalmente digitou um número positivo, portanto
        // mostramos a ele a metade do número informado
        printf("Metade do número informado: %i\n\n", numInformado/2);
    }

    // Terminar a execução do programa
    return 0;
}
