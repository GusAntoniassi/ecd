/* Entrar com o nome, idade (bloquear os números negativos com estrutura de repetição
while) e sexo (bloquear sexo diferente de F/f/M/m com estrutura de repetição Do...While) de
5 pessoas. Imprimir o nome e a idade se a pessoa for do sexo masculino e tiver mais de 21
anos */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos
#include <conio.h> // biblioteca para usar o getche

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    char nome[80]; // Nome da oessoa
    int idade; // Idade da pessoa (maior que 0)
    char sexo; // Sexo da pessoa ('F', 'f', 'M' ou 'm')
    int cont = 0; // Contador que irá de 0 até 5

    // Laço que repetirá 5 vezes para entrar com as pessoas
    for (cont = 0; cont < 5; cont++) {
        // Perguntar e ler o nome da pessoa
        printf("Informe o nome:\n");
        fflush(stdin); // comando necessário para limpar o buffer do teclado e evitar problemas
        gets(nome);

        // Perguntar e ler a idade da pessoa
        printf("Informe a idade:\n");
        // Laço que repetirá até que a idade informada seja maior que 0
        do {
            // Ler a idade
            scanf("%i", &idade);
            // Condição para mostrar um aviso ao usuário se ele entrar com um número negativo
            if (idade < 0) {
                printf("Idade negativa informada! Tente novamente:\n");
            }
        } while (idade < 0);

        // Perguntar e ler o sexo da pessoa
        printf("Informe o sexo:\n");
        do {
            fflush(stdin); // comando necessário para limpar o buffer do teclado e evitar problemas
            sexo = getche();

            // Condição para mostrar um aviso ao usuário se ele entrar com um sexo diferente de 'F', 'f', 'M' ou 'm'
            if (sexo != 'F' && sexo != 'f' && sexo != 'M' && sexo != 'm') {
                printf("Sexo inválido informado! Tente novamente:\n");
            }
        } while (sexo != 'F' && sexo != 'f' && sexo != 'M' && sexo != 'm');

        // Se a idade da pessoa for maior que 21, imprimir o nome e a idade conforme solicitado
        if (idade > 21) {
            printf("Pessoa tem mais de 21 anos!\n");
            printf("O nome é: %s\n", nome);
            printf("A idade é: %i\n", idade);
        }
        // Pular uma linha para melhorar a apresentação do programa
        printf("\n");
    }

    // Terminar a execução do programa
    return 0;
}
