/* Entre com 15 números positivos (bloquear os números negativos com estrutura de repetição
while) e ao final imprima a média dos números lidos. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont = 0; // contador que irá de 0 a 15
    int numInformado = 0; // número que o usuário irá digitar
    int somatorio = 0; // somatório dos números informados
    float media = 0.0; // média dos números informados

    // Laço que repetirá 15 vezes para ler todos os números
    for (cont = 0; cont < 15; cont++) {
        // Pedir o número ao usuário
        printf("Entre com um numero:\n");

        // Laço que repetirá até que o usuário entre com um número positivo
        do {
            // Ler o número do usuário
            scanf("%i", &numInformado);

            // Condicional para mostrar mensagem de erro ao usuário se ele digitar um número negativo
            if (numInformado < 0) {
                printf("Erro: Número negativo informado! Tente novamente:\n");
            }
        } while (numInformado < 0);

        // Adicionar o número lido no somatório
        somatorio += numInformado;
    }

    // Após a leitura dos 15 números, fazer o cálculo da média
    media = somatorio / 15.0;
    // Mostrar a média para o usuário
    printf("A média dos números informados é: %0.2f", media);
    // Terminar a execução do programa
    return 0;
}
