/* Elabore um algoritmo que imprima todos os números compreendidos entre -10 e +30 */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont = 0; // contador

    printf("Números compreendidos entre -10 e +30:\n");
    // Laço que vai começar em -10 e parar depois do 30, incrementando o contador a cada iteração
    for (cont = -10; cont <= 30; cont++) {
        // Imprimir o contador para o usuário
        printf("%i\n", cont);
    }

    // Terminar a execução do programa
    return 0;
}
