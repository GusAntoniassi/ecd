/*Elabore um algoritmo que imprima todos os números ímpares existentes entre N1 e N2, em
que N1 e N2 são números naturais fornecidos pelo usuário. Se N2 for menor que N1 faça a
impressão do maior para o menor.*/

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont, n1, n2;

    // Pegar o valor do início da faixa de números a ser trabalhada (N1)
    printf("Insira o valor do início da faixa:\n");
    scanf("%i", &n1);

    // Pegar o valor do fim da faixa de números a ser trabalhada (N2)
    printf("Insira o valor do fim da faixa:\n");
    scanf("%i", &n2);

    printf("Números ímpares entre %i e %i:\n", n1, n2);
    // Estrutura que irá repetir de N1 até N2
    for (cont = n1; cont < n2; cont++) {
        // Se o resto da divisão do número por 2 for igual a 1, então o número é ímpar
        if (cont % 2 == 1) {
            // Mostrar o número para o usuário
            printf("%i\n", cont);
        }
    }

    // Terminar a execução do programa
    return 0;
}
