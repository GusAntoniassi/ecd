/* Elabore um algoritmo que imprima o dobro dos números compreendidos entre 100 e 20 na
ordem decrescente (do maior para o menor). */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont = 0; // contador

    // Estrutura que iniciará em 100, e terminará quando o contador for menor que 20, decrementando-o a cada iteração
    printf("Dobro dos números compreendidos entre 100 e 200 na ordem decrescente:\n");
    for (cont = 100; cont >= 20; cont--) {
        // Mostrar o dobro do contador
        printf("%i\n", cont * 2);
    }

    // Terminar a execução do programa
    return 0;
}
