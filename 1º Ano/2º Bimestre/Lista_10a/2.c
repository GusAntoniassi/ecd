/* A conversão de graus Fahrenheit para centígrados é obtida por C := (5/9)*(F-32). Escreva
um algoritmo que calcule uma tabela de graus centígrados em função de graus Fahrenheit que
variem de 70 até 90. */

#include <stdio.h> // biblioteca padrão
#include <locale.h> // biblioteca para usar acentos

int main() {
    // Permitir o uso de caracteres especiais (acentos)
    setlocale(LC_ALL, "");

    // Declaração de variáveis
    int cont = 0; // contador
    float fahrenheit = 0, centigrados = 0; // variáveis usadas na conversão

    printf("Tabela de graus centígrados em função de graus Fahrenheit que variam de 70 até 90:\n");
    // Laço que vai começar em 70 e parar depois do 90, incrementando o contador a cada iteração
    for (cont = 70; cont <= 90; cont++) {
        // O valor de fahrenheit será sempre o mesmo que o do contador, pois a tabela vai de 70 até 90
        fahrenheit = cont;

        // Fazer a conversão usando a fórmula passada (temos que usar 5.0 e 9.0 ou o resultado da divisão será 0, e não 0,55555)
        centigrados = (5.0/9.0) * (fahrenheit-32);

        // Mostrar o valor em Fahrenheit, seguido de um "°F", e depois o valor em centígrados, seguido de um "°C"
        printf("%0.0f°F = %0.2f°C\n", fahrenheit, centigrados);
    }

    // Terminar a execução do programa
    return 0;
}
