/*3) Solicitando ao usuário 8 valores, os quais devem ser armazenados num único
vetor, apresente o maior valor e a posição em que o mesmo foi informado.*/

#include <stdio.h>

int main() {
    int vetor[8]; // Vetor de 8 posições
    int maiorValor = 0; // Por enquanto o maior valor é 0
    int indiceDoMaior; // E a posição do maior valor não existe ainda
    int i; // Contador

    for (i = 0; i < 8; i++) {
        // Lemos o valor
        printf("vetor[%i] = ", i);
        scanf("%i", &vetor[i]);

        // Se o valor lido é maior que o valor armazenado em maiorValor (que na primeira vez sempre vai ser 0)
        if (vetor[i] > maiorValor) {
            // Armazenamos o valor lido no maiorValor
            maiorValor = vetor[i];
            // Guardamos a posição que ele foi informado
            indiceDoMaior = i;
        }
    }

    printf("Maior valor: vetor[%i] = %i", indiceDoMaior, maiorValor);
    return 0;
}
