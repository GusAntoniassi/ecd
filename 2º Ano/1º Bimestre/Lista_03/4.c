/*4) Achar a somatória de cada uma das linhas de uma matriz (7x5), listando no final
a somatória de todas as linhas.*/

#include <stdio.h>

int main() {
    int matriz[7][5]; // Matriz 7x5
    int somatoriaLinha = 0, // Somatória de cada linha, por enquanto é 0
    somatoriaGeral = 0; // Somatória de todos os valores, por enquanto é 0
    int col, li; // Contadores

    // Laço que vai passar por cada linha da matriz
    for (li = 0; li < 7; li++) {
        // A cada linha que lermos, temos que zerar a somatória de linhas
        somatoriaLinha = 0;

        // Laço que vai passar por cada coluna da matriz
        for (col = 0; col < 5; col++) {
            // Ler um número
            printf("matriz[%i][%i] = ", li, col);
            scanf("%i", &matriz[li][col]);

            // Adicionar o número lido na somatória da linha
            somatoriaLinha += matriz[li][col];
        }
        // Após repetir por toda a coluna, adicionar a somatória da linha na somatória geral
        somatoriaGeral += somatoriaLinha;

        // Imprimimos a somatória antes de ir para a próxima linha
        printf("Somatória da linha %i: %i\n", li, somatoriaLinha);
    }

    printf("Somatória geral: %i", somatoriaGeral);
    return 0;
}
