/*1) Num único vetor de 6 posições, entre com os valores e ao final mostre a
somatória dos valores armazenados nas respectivas posições.*/

#include <stdio.h>
#include <locale.h> // Biblioteca para usar acentuação

int main() {
    // Instrução p/ usar acentuação no programa
    setlocale(LC_ALL, "");

    int vetor[6]; // Vetor de 6 posições
    int somatoria = 0; // Variável da somatória, que por enquanto é 0
    int i; // Contador

    for (i = 0; i < 6; i++) {
        // Aqui imprimimos algo como "vetor[1] = " para o usuário saber em qual índice ele está
        printf("vetor[%i] = ", i);
        // Armazenamos o número entrado o índice que o contador está
        scanf("%i", &vetor[i]);

        // Acrescentamos o valor lido na somatória
        somatoria += vetor[i];
    }

    // Após ler todos os 6 valores, imprimimos a somatória
    printf("Somatória: %i", somatoria);

    return 0;
}
