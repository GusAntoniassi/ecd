/* Esse é um método auxiliar para imprimir o conteúdo de um vetor. Nada mais é que um laço de repetição simples, para imprimir cada elemento separado por um espaço.
Para implementá-lo basta copiar a função para o seu programa e chamá-la, seguindo o exemplo abaixo. */

#include <stdio.h>

void imprimirVetor(int tamanho, int arr[tamanho]) {
    int i;

    printf("\n");
    for (i = 0; i < tamanho; i++) {
        printf("%i\t", arr[i]);
    }
}

int main() {
    // Definir as variáveis
    int i;
    int vetor[5];

    // Atribuir os valores
    for (i = 0; i < 5; i++) {
        vetor[i] = i;
    }

    // Imprimir o vetor
    printf("Vetor:");
    imprimirVetor(5, vetor); // Utilização: imprimirVetor(tamanho, vetor)

    // INCORRETO: imprimirVetor(5, vetor[5]);
    // CORRETO: imprimirVetor(5, vetor);
}
