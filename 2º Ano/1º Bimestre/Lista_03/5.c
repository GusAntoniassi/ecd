/*5) Dada uma matriz de (7x7), separar os elementos da diagonal principal
(esquerda para a direita) e armazene num vetor. No final liste a somatória dos
valores inseridos no vetor*/

#include <stdio.h>

int main() {
    int matriz[7][7], diagonal[7]; // A diagonal será um vetor, e não uma matriz
    int somatoriaDiagonal = 0; // Somatória dos números na diagonal
    int li, col, i; // Contadores

    // Laço que vai passar por cada linha da matriz
    for (li = 0; li < 7; li++) {
        // Laço que vai passar por cada coluna da matriz
        for (col = 0; col < 7; col++) {
            // Ler o valor
            printf("matriz[%i][%i] = ", li, col);
            scanf("%i", &matriz[li][col]);

            /* Se o número da linha for igual ao número da coluna, então estamos na diagonal
            Ex:
                   [1] [2] [3] [4]
               [1]  1   2   3   4
               [2]  1   2   3   4
               [3]  1   2   3   4
               [4]  1   2   3   4

            Repare que os valores na diagonal principal sempre estão em índices iguais na horizontal e vertical,
            como (1, 1), (2, 2), e assim por diante
            */

            if (li == col) {
                // Aqui tanto faz diagonal[li] ou diagonal[col], já que se estamos aqui dentro, os dois valores são iguais
                diagonal[li] = matriz[li][col];
            }
        }
        printf("\n");
    }

    // Listar os itens da diagonal e somá-los na somatória
    printf("Itens da diagonal: \n");
    for (i = 0; i < 7; i++) {
        printf("%i ", diagonal[i]);
        somatoriaDiagonal += diagonal[i];
    }

    // Imprimir a somatória
    printf("\nSomatória da diagonal: \n%i", somatoriaDiagonal);
    return 0;
}
