/*2) Leia valores para dois vetores com capacidade de armazenar 5 números inteiro
(cada um) e armazene a soma deles num terceiro vetor, imprimindo a soma
parcial (soma dos valores lidos nos dois vetores a cada leitura) e no final mostre a
soma total dos valores acumulados no terceiro vetor.*/

#include <stdio.h>

int main() {
    int vetor1[5], vetor2[5], soma[5]; // Vetores utilizados no exercício
    int somaTotal; // Por enquanto a soma total dos valores é 0
    int i; // Contador


    for (i = 0; i < 5; i++) {
        // Ler o valor do primeiro vetor
        printf("vetor1[%i] = ", i);
        scanf("%i", &vetor1[i]);

        // Ler o valor do segundo vetor
        printf("vetor2[%i] = ", i);
        scanf("%i", &vetor2[i]);

        // Armazenar a soma dos dois no vetor de soma
        soma[i] = vetor1[i] + vetor2[i];

        // Imprimimos a soma dos dois vetores para o usuário
        printf("vetor1[%i] + vetor2[%i] = %i\n", i, i, soma[i]);

        // Calculamos a soma total dos valores no terceiro vetor
        somaTotal += soma[i];
    }

    // Imprimimos o valor para o usuário
    printf("Soma total: %i", somaTotal);
    return 0;
}
