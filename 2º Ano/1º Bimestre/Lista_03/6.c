/*6) Faça um algoritmo que carregue uma matriz 3x3 e no final imprima o maior
elemento dessa matriz.*/

#include <stdio.h>

int main() {
    int matriz[3][3]; // Matriz
    int maior = 0; // Por enquanto, o maior valor é 0
    int li, col; // Contadores

    // Laço que vai passar por cada linha da matriz
    for (li = 0; li < 3; li++) {
        // Laço que vai passar por cada coluna da matriz
        for (col = 0; col < 3; col++) {
            // Ler o valor
            printf("matriz[%i][%i] = ", li, col);
            scanf("%i", &matriz[li][col]);

            // Se o valor lido for maior que o valor armazenado na variável do maior...
            if (matriz[li][col] > maior) {
                // Atribuimos o valor lido à variável de maior
                maior = matriz[li][col];
            }
        }
    }

    // Após passar por todos os valores, imprimir o maior deles
    printf("O maior número da matriz é: %i", maior);
    return 0;
}
