/* Esse é um método auxiliar para imprimir o conteúdo de uma matriz na forma de uma tabela. Imprimimos cada elemento da linha separado por um \t, e depois ao final da linha imprimimos um \n.
Para implementá-lo basta copiar a função para o seu programa e chamá-la, seguindo o exemplo abaixo. */

#include <stdio.h>

void imprimirMatriz(int linhas, int colunas, int arr[linhas][colunas]) {
    int i, j;

    for (i = 0; i < linhas; i++) {
        printf("\n");
        for (j = 0; j < colunas; j++) {
            printf("%i\t", arr[i][j]);
        }
    }
}

int main() {
    // Definir as variáveis
    int i, j;
    int matriz[5][5];

    // Ler os valores
    for (i = 0; i < 5; i++) {
        for (j = 0; j < 5; j++) {
            matriz[i][j] = i+j;
        }
    }

    // Imprimir a matriz
    printf("Matriz:");
    imprimirMatriz(5, 5, matriz); // Utilização: imprimirMatriz(número de linhas, número de colunas, matriz);

    // INCORRETO: imprimirMatriz(5, 5, matriz[5]) ou imprimirMatriz(5, 5, matriz[5][5])
    // CORRETO: imprimirMatriz(5, 5, matriz);
}
