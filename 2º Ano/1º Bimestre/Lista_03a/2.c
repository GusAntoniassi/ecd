/* Elabore um programa utilizando matriz que faça a leitura de 4 notas
referentes a 8 alunos. Num segundo momento calcule a média de
cada aluno armazenando essa média na matriz. Posteriormente
mostre a média de cada aluno dizendo se o aluno esta ou não
aprovado. Ao final mostrar a maior media e a menor média. */

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int main() {
    setlocale(LC_ALL, "");
    float notas[8][5]; // 8 alunos e 4 notas (+ 1 campo para a média)
    float maiorMedia, menorMedia;
    int i, j;

    // Percorrer todos os alunos
    for (i = 0; i < 8; i++) {
        // A soma e a média serão zeradas para cada aluno
        float soma = 0, media = 0;

        // Percorrer as 4 notas
        for (j = 0; j < 4; j++) {
            printf("Insira a %iª nota, referente ao %iº aluno: ", j+1, i+1);
            scanf("%f", &notas[i][j]);
            soma += notas[i][j];
        }

        // Calcular a média
        media = soma / 4;
        // Armazenar a média dentro do vetor (como o índice começa em 0, temos que usar 4 e não 5)
        notas[i][4] = media;

        // Se for a primeira média, ela obrigatoriamente vai ser a menor
        // Ou então se a média for menor que a menor média que já armazenamos, temos que atualizar o valor
        if (i == 0 || media < menorMedia) {
            menorMedia = media;
        }

        // Se a média for maior que a maior média que já armazenamos, temos que atualizar o valor
        if (media > maiorMedia) {
            maiorMedia = media;
        }

        // Exibir a média após inserir as notas
        printf("Média: %0.1f\n\n", media);
    }

    // Relatório
    system("clear");

    // Percorrer todos os alunos, mostrando a média de cada
    for (i = 0; i < 8; i++) {
        printf("Aluno %i - Média %0.1f\n", i+1, notas[i][4]);
    }

    // Mostrar a maior e menor média
    printf("\n");
    printf("Maior média: %0.1f\n", maiorMedia);
    printf("Menor média: %0.1f", menorMedia);


    return 0;
}
