/*Elabore um programa utilizando vetor onde, numa linha de
montagem de computadores são montados uma certa quantidade de
máquinas por dia. Faça uma algoritmo utilizando vetor que armazene
o numero de computadores montados por dia (levar em consideração
que o mês tem 30 dias). Num segundo momento mostre:
- A quantidade de montagem no mês;
- O dia em que houve mais montagem e menos montagem;
- Media de montagem por dia.*/

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int main() {
    // Utilizar acentos
    setlocale(LC_ALL, "");

    int montagem[30];
    int i,         // Contador
        maior = 0, // Maior quantidade de computadores montados em um dia
        diaMaior,  // Dia com a maior quantidade
        menor = 0, // Menor quantidade de computadores montados em um dia
        diaMenor,  // Dia com a menor quantidade
        soma = 0;  // Soma de todos os computadores montados
    float media;   // Média de computadores por dia

    // Estrutura que vai percorrer todos os dias no mês
    for (i = 0; i < 30; i++) {
        // Usamos i+1 porque nosso vetor começa no 0 e termina no 29, mas o mês começa no dia 1 e termina no dia 30
        printf("Insira a quantidade de computadores montados no dia %i: ", i+1);
        scanf("%i", &montagem[i]);

        // Se for o primeiro dia, ele obrigatoriamente será o dia com o menor número de montagens
        // Senão, temos que ver se o total de montagens é menor que o menor valor que já armazenamos
        if (i == 0 || montagem[i] < menor) {
            // Armazenar o valor e o dia
            menor = montagem[i];
            diaMenor = i+1; // Para transformar o índice (0-29) em dia (1-30), temos que somar +1
        }

        // Se o total de montagens é maior que o maior valor que já armazenamos
        if (montagem[i] > maior) {
            // Armazenar o valor e o dia
            maior = montagem[i];
            diaMaior = i+1;
        }

        // Acrescentar na somatória o total de montagens deste dia
        soma += montagem[i];
    }

    // Calculamos a média (usando 30.0 para obter um resultado decimal, em vez de um inteiro)
    media = soma / 30.0;

    // Limpar a tela
    system("cls");

    // Mostrar o relatório
    printf("Quantidade de montagem no mês: %i computadores\n", soma);
    printf("Dia com mais montagens: dia %i (%i computadores)\n", diaMaior, maior);
    printf("Dia com menos montagens: dia %i (%i computadores)\n", diaMenor, menor);
    printf("Média de montagem por dia: %0.1f", media);

    // Sair do programa
    return 0;
}
